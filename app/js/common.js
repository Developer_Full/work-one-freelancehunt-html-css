$(function() {

});
// ================= Смена изображений для примеры работ
function l_image(a) {
	document.example_img.src=a
}


// ================ Показ сертификатов
// 
var SertificatDot1 = $('.sertificats-dot:nth-child(1)');
var SertificatDot2 = $('.sertificats-dot:nth-child(2)');
var SertificatDot3 = $('.sertificats-dot:nth-child(3)');

var Sertificat1 = $('.sertificat-image:nth-child(1)').children('img');
var Sertificat2 = $('.sertificat-image:nth-child(2)').children('img');
var Sertificat3 = $('.sertificat-image:nth-child(3)').children('img');

$('.sertificats-dot:nth-child(1)').click(function(){
	$('.sertificat-image:nth-child(1)').children('img').css("display" , "block")
	$('.sertificat-image:nth-child(2)').children('img').css("display" , "none")
	$('.sertificat-image:nth-child(3)').children('img').css("display" , "none")
})

$('.sertificats-dot:nth-child(2)').click(function(){
	$('.sertificat-image:nth-child(2)').children('img').css("display" , "block")
	$('.sertificat-image:nth-child(1)').children('img').css("display" , "none")
	$('.sertificat-image:nth-child(3)').children('img').css("display" , "none")
})

$('.sertificats-dot:nth-child(3)').click(function(){
	$('.sertificat-image:nth-child(3)').children('img').css("display" , "block")
	$('.sertificat-image:nth-child(1)').children('img').css("display" , "none")
	$('.sertificat-image:nth-child(2)').children('img').css("display" , "none")
})

// =============== Отзывы
// 
var ReviewDot1 = $('.reviews-dot:nth-child(1)');
var ReviewDot2 = $('.reviews-dot:nth-child(2)');
var ReviewDot3 = $('.reviews-dot:nth-child(3)');

var Review1 = $('.reviews-container:nth-child(1)');
var Review2 = $('.reviews-container:nth-child(2)');
var Review3 = $('.reviews-container:nth-child(3)');

$('.reviews-dot:nth-child(1)').click(function(){
	$('.reviews-container:nth-child(1)').css("display" , "block")
	$('.reviews-container:nth-child(2)').css("display" , "none")
	$('.reviews-container:nth-child(3)').css("display" , "none")
})

$('.reviews-dot:nth-child(2)').click(function(){
	$('.reviews-container:nth-child(2)').css("display" , "block")
	$('.reviews-container:nth-child(1)').css("display" , "none")
	$('.reviews-container:nth-child(3)').css("display" , "none")
})

$('.reviews-dot:nth-child(3)').click(function(){
	$('.reviews-container:nth-child(3)').css("display" , "block")
	$('.reviews-container:nth-child(1)').css("display" , "none")
	$('.reviews-container:nth-child(2)').css("display" , "none")
})


// ========= Одинаковая высота у элементов
var RightHeight = $('.right-content').height();
var LeftBlock = $('.left-aside');
LeftBlock[0].style.height = RightHeight + 'px'

// ========= Пишем стили для Radio Button

function changeChekedOne() {
	$('.radio-block__one').css('background' , '#0489b2')
	$('.radio-block__two').css('background' , '#fff')
	$('.radio-block__one').css('border' , 'none')
	$('.radio-block__two').css('border' , '2px solid #95989A')
}

function changeChekedTwo() {
	$('.radio-block__two').css('background' , '#0489b2');
	$('.radio-block__one').css('background' , '#fff');
	$('.radio-block__one').css('border' , '2px solid #95989A')
	$('.radio-block__two').css('border' , 'none')

}

// // ========== Автоматическая ширина для правой колонки
// var TotalWidth = window.innerWidth;
// var LeftContentWidth = $('.left-content').width();
// var RightContentWidth = TotalWidth - LeftContentWidth;
// $('.right-content').css('width' , RightContentWidth)